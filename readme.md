Nama : Arnoldy Fatwa Rahmadin

NIM : 222011330

Kelas : 3SI3

Praktikum Pemrograman Platform Khusus Pertemuan 5

1. Pengujian Register
![logo](/Dokumentasi%20Praktikum/Screenshot%20(89).png)

2. Pengujian Login
![logo](/Dokumentasi%20Praktikum/Screenshot%20(91).png)

3. Get Current User Login
![logo](/Dokumentasi%20Praktikum/Screenshot%20(92).png)

4. Access Protected Resources
![logo](/Dokumentasi%20Praktikum/Screenshot%20(93).png)

(Jika mengakses tanpa TOKEN)

akan muncul permintaan "Token Required"
![logo](/Dokumentasi%20Praktikum/Screenshot%20(94).png)

(Jika mengakses dengan TOKEN salah)

akan muncul notifikasi "Invalid Token"
![logo](/Dokumentasi%20Praktikum/Screenshot%20(95).png)
